/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_hosttools.h>
#include <buildboxrun_hosttools_changedirectoryguard.h>
#include <buildboxrun_hosttools_pathprefixutils.h>

#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_runner.h>

#include <stdlib.h>
#include <unistd.h>

namespace buildboxcommon {
namespace buildboxrun {
namespace hosttools {

std::string join(const std::vector<std::string> &tokens,
                 const std::string &spacer)
{
    if (tokens.empty()) {
        return "";
    }

    std::ostringstream ss;
    bool first = true;
    for (const std::string &token : tokens) {
        if (first) {
            ss << token;
            first = false;
        }
        else {
            ss << spacer << token;
        }
    }
    return ss.str();
}

void HostToolsRunner::setUpEnvironment(const Command &command) const
{
    for (const auto &envVar : command.environment_variables()) {
        if (setenv(envVar.name().c_str(), envVar.value().c_str(), 1) == -1) {
            BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
                std::system_error, errno, std::system_category,
                "Unable to set environment " << envVar.name() << "="
                                             << envVar.value());
        }
    }
}

void HostToolsRunner::createParentDirectories(
    const Command &command, const std::string &workingDir) const
{
    BUILDBOX_RUNNER_LOG(
        DEBUG, "Creating parent directories for Command output files");
    for (const std::string &file : command.output_files()) {
        if (file.find("/") != std::string::npos) {
            FileUtils::createDirectory(
                (workingDir + "/" + file.substr(0, file.rfind("/"))).c_str());
        }
    }
    BUILDBOX_RUNNER_LOG(DEBUG,
                        "Created parent directories for Command output files");

    BUILDBOX_RUNNER_LOG(
        DEBUG, "Creating parent directories for Command output directories");
    for (const std::string &directory : command.output_directories()) {
        if (directory.find("/") != std::string::npos) {
            FileUtils::createDirectory(
                (workingDir + "/" + directory.substr(0, directory.rfind("/")))
                    .c_str());
        }
    }
    BUILDBOX_RUNNER_LOG(
        DEBUG, "Created parent directories for Command output directories");
}

std::vector<std::string>
HostToolsRunner::generateCommandLine(const Command &command,
                                     const std::string &stage_path) const
{
    const std::vector<std::string> commandLine(command.arguments().cbegin(),
                                               command.arguments().cend());
    if (this->d_prefix) {
        return PathPrefixUtils::prefixAbspathsWithStagedDir(commandLine,
                                                            stage_path);
    }

    return commandLine;
}

ActionResult HostToolsRunner::execute(const Command &command,
                                      const Digest &inputRootDigest)
{
    ActionResult result;
    auto *result_metadata = result.mutable_execution_metadata();

    Runner::metadata_mark_input_download_start(result_metadata);
    const auto stagedDir = this->stageDirectory(inputRootDigest);

    // If `stage()` fails, it'll log the error and throw.
    Runner::metadata_mark_input_download_end(result_metadata);

    std::ostringstream workingDir;
    workingDir << stagedDir->getPath() << "/" << command.working_directory();

    {
        ChangeDirectoryGuard g(workingDir.str());
        setUpEnvironment(command);
        createParentDirectories(command, workingDir.str());

        const std::vector<std::string> commandLine =
            generateCommandLine(command, stagedDir->getPath());

        BUILDBOX_RUNNER_LOG(DEBUG, "Executing " << join(commandLine, " "));
        executeAndStore(commandLine, &result);
    }

    const auto signal_status = Runner::getSignalStatus();
    if (signal_status) {
        // If signal is set here, remove working directory and exit
        BUILDBOX_RUNNER_LOG(DEBUG, "Removing " << workingDir.str());
        buildboxcommon::FileUtils::deleteDirectory(workingDir.str().c_str());
        exit(Runner::getSignalStatus());
    }

    BUILDBOX_RUNNER_LOG(DEBUG, "Capturing command outputs from \""
                                   << workingDir.str() << "\"...");
    Runner::metadata_mark_output_upload_start(result_metadata);
    stagedDir->captureAllOutputs(command, &result);
    Runner::metadata_mark_output_upload_end(result_metadata);

    // Set permissions of the stage dir to 0777.
    buildboxcommon::Runner::recursively_chmod_directories(stagedDir->getPath(),
                                                          0777);
    BUILDBOX_RUNNER_LOG(DEBUG, "Finished capturing command outputs");
    return result;
}

bool HostToolsRunner::parseArg(const char *arg)
{
    assert(arg);
    if (arg[0] == '-' && arg[1] == '-') {
        arg += 2;
        if (strcmp(arg, "prefix-staged-dir") == 0) {
            this->d_prefix = true;
            return true;
        }
    }
    return false;
}

} // namespace hosttools
} // namespace buildboxrun
} // namespace buildboxcommon
