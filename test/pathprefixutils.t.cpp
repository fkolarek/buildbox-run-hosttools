/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_hosttools_pathprefixutils.h>

#include <gtest/gtest.h>

using namespace buildboxcommon;
using namespace buildboxrun;
using namespace hosttools;

TEST(PrefixArgumentTests, PrefixStagedDir)
{
    std::vector<std::string> prefixes = {
        "-include",   "-imacros", "-I",        "-iquote",   "-isystem",
        "-idirafter", "-iprefix", "-isysroot", "--sysroot="};
    for (const std::string prefix : prefixes) {
        const std::string output = PathPrefixUtils::prefixArgWithStagedDir(
            prefix + "/absolute/path", "/staged/dir");
        EXPECT_EQ(prefix + "/staged/dir/absolute/path", output);
    }

    EXPECT_EQ("/staged/dir/absolute/path",
              PathPrefixUtils::prefixArgWithStagedDir("/absolute/path",
                                                      "/staged/dir"));
}

// This case realistically won't happen, since the
// files will be installed somewhere, but let's make
// sure we don't die horribly if we try.
TEST(PrefixArgumentTests, EmptyStagedDirectory)
{
    EXPECT_EQ("-I/foo", PathPrefixUtils::prefixArgWithStagedDir("-I/foo", ""));
    EXPECT_EQ("-Ifoo", PathPrefixUtils::prefixArgWithStagedDir("-Ifoo", ""));
}

TEST(PrefixCommandTests, PrefixCommandSet)
{
    std::vector<std::string> command = {"g++",           "-I/test", "-c",
                                        "/tmp/fakefile", "-o",      "foo.out"};
    std::vector<std::string> expected = {"g++", "-I/staged/dir/test",
                                         "-c",  "/staged/dir/tmp/fakefile",
                                         "-o",  "foo.out"};
    std::vector<std::string> result =
        PathPrefixUtils::prefixAbspathsWithStagedDir(command, "/staged/dir");
    EXPECT_EQ(expected, result);
}

TEST(PrefixCommandTests, PrefixCommandSetEmptyStagedDir)
{
    std::vector<std::string> command = {"g++",      "-I/test", "-c",
                                        "fakefile", "-o",      "foo.out"};
    std::vector<std::string> result =
        PathPrefixUtils::prefixAbspathsWithStagedDir(command, "");
    EXPECT_EQ(command, result);
}

TEST(PrefixCommandTests, DontPrefixCompiler)
{
    std::vector<std::string> command = {"/usr/bin/g++",  "-I/test", "-c",
                                        "/tmp/fakefile", "-o",      "foo.out"};
    std::vector<std::string> expected = {
        "/usr/bin/g++", "-I/staged/dir/test",
        "-c",           "/staged/dir/tmp/fakefile",
        "-o",           "foo.out"};
    std::vector<std::string> result =
        PathPrefixUtils::prefixAbspathsWithStagedDir(command, "/staged/dir");
    EXPECT_EQ(expected, result);
}

TEST(PrefixCommandTests, OnlyCompiler)
{
    std::vector<std::string> command = {"/usr/bin/g++"};
    std::vector<std::string> result =
        PathPrefixUtils::prefixAbspathsWithStagedDir(command, "/staged/dir");
    EXPECT_EQ(command, result);
}
